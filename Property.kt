package com.netgalaxystudios.window

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Location
import android.util.Log
import com.parse.*
import io.paperdb.Paper
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.ByteArrayOutputStream
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by charlesvonlehe on 6/22/17.
 */

class Property(parseObject: ParseObject) {
    var objectId = ""
    var title = ""
    var location: Location? = null
    var receiverObjectId: String? = null
    var senderObjectIds = ArrayList<String>()
    private var imageFile: ParseFile? = null
    private var image: ByteArray? = null
    var address: String? = null
    var atLocationObjectIds = ArrayList<String>()
    private var receiver: User? = null

    init {
        if (parseObject.objectId != null) {
            objectId = parseObject.objectId
        }
        if (parseObject.get(Constants.PFColumns.Property.title) != null) {
            title = parseObject.getString(Constants.PFColumns.Property.title)
        }
        if (parseObject.get(Constants.PFColumns.Property.location) != null) {
            val loc = Location(objectId)
            loc.latitude = parseObject.getParseGeoPoint(Constants.PFColumns.Property.location).latitude
            loc.longitude = parseObject.getParseGeoPoint(Constants.PFColumns.Property.location).longitude
            location = loc
        }
        if (parseObject.get(Constants.PFColumns.Property.receiverObjectId) != null) {
            receiverObjectId = parseObject.getString(Constants.PFColumns.Property.receiverObjectId)
        }
        if (parseObject.get(Constants.PFColumns.Property.senderObjectIds) != null) {
            senderObjectIds.clear()
            senderObjectIds.addAll(parseObject.getList<String>(Constants.PFColumns.Property.senderObjectIds))
        }
        if (parseObject.get(Constants.PFColumns.Property.imageFile) != null) {
            imageFile = parseObject.getParseFile(Constants.PFColumns.Property.imageFile)
        }
        if (parseObject.get(Constants.PFColumns.Property.address) != null) {
            address = parseObject.getString(Constants.PFColumns.Property.address)
        }
        if (parseObject.get(Constants.PFColumns.Property.atLocationObjectIds) != null) {
            atLocationObjectIds.clear()
            atLocationObjectIds.addAll(parseObject.getList<String>(Constants.PFColumns.Property.atLocationObjectIds))
        }
    }

    fun checkIfStillSender(forUser: User, completion: (isSender: Boolean) -> Unit) {
        val query = getQuery()
        query.whereEqualTo(Constants.PFColumns.objectId, objectId)
        query.whereEqualTo(Constants.PFColumns.Property.senderObjectIds, forUser.objectId)
        query.countInBackground { count, e ->
            completion(count > 0)
        }
    }

    fun addSender(user: User, completion: (success: Boolean) -> Unit) {
        val parseObject = ParseObject.createWithoutData(Constants.PFColumns.Property.className, objectId)
        parseObject.addUnique(Constants.PFColumns.Property.senderObjectIds, user.objectId)
        parseObject.saveInBackground { e ->
            if (e == null && !senderObjectIds.contains(user.objectId)) {
                senderObjectIds.add(user.objectId)
            }
            completion(e == null)
        }
    }

    fun followingMeSearchText(text:String, completion:(containsText:Boolean?) -> Unit): Boolean{
        getReceiver { receiver ->
            if (receiver != null){
                if (receiver.firstName.toLowerCase().contains(text.toLowerCase(), true) || receiver.lastName.toLowerCase().contains(text.toLowerCase(), true) ||  receiver.email.toLowerCase().contains(text.toLowerCase(), true)){
                    completion(true)
                }
                if (title.toLowerCase().contains(text.toLowerCase(), true)) {
                    completion(true)
                }
                if (address != null && address!!.toLowerCase().contains(text.toLowerCase(), true)) {
                    completion(true)
                }
            } else {
                completion(null)
            }
        }
        return true
    }

    fun containsSearchText(text: String): Boolean {
        println("TITLE: " + title + " TEXT: " + text)
        if (title.toLowerCase().contains(text.toLowerCase(), true)) {
            return true
        }
        if (address != null && address!!.toLowerCase().contains(text.toLowerCase(), true)) {
            return true
        }
        return false
    }

    fun removeSender(user: User, completion: (success: Boolean) -> Unit) {
        val parseObject = ParseObject.createWithoutData(Constants.PFColumns.Property.className, objectId)
        val senderObjIds = ArrayList<String>()
        val atLocationObjIds = ArrayList<String>()

        senderObjIds.addAll(senderObjectIds)
        senderObjIds.remove(user.objectId)
        atLocationObjIds.addAll(atLocationObjectIds)
        atLocationObjIds.remove(user.objectId)
        parseObject.put(Constants.PFColumns.Property.senderObjectIds, senderObjIds)
        parseObject.put(Constants.PFColumns.Property.atLocationObjectIds, atLocationObjIds)
        parseObject.saveInBackground { error ->
            Unit
            if (error == null) {
                senderObjectIds.remove(user.objectId)
                atLocationObjectIds.remove(user.objectId)
                completion(true)
            } else {
                completion(false)
            }
        }


    }

    fun delete(completion: (success: Boolean) -> Unit) {
        ParseObject.createWithoutData(Constants.PFColumns.Property.className, objectId).deleteInBackground { error ->
            Unit
            completion(error == null)
        }
    }

    fun getReceiver(completion: (receiver: User?) -> Unit) {
        if (receiver != null) {
            completion(receiver)
        } else if (receiverObjectId != null) {
            User.getUserForObjectId(receiverObjectId!!, { user: User? -> Unit
                receiver = user
                completion(receiver)
            })
        } else {
            completion(null)
        }
    }

    fun getSenders(completion: (senders: ArrayList<User>) -> Unit) {
        val query = ParseUser.getQuery()
        query.whereContainedIn(Constants.PFColumns.objectId, senderObjectIds)
        query.findInBackground { objects, e ->
            val senders = ArrayList<User>()
            if (objects != null) {
                for (userObject in objects) {
                    senders.add(User(userObject))
                }
            }
            completion(senders)
        }
    }

    fun getImage(completion: (image: ByteArray?) -> Unit) {
        println("getImage1")
        if (image != null) {
            println("getImage2")
            completion(image)
        } else if (imageFile != null) {
            println("getImage3")
            imageFile?.getDataInBackground { data: ByteArray?, e: ParseException? ->
                Unit
                if (data != null) {
                    println("getImage4: " + image)
                    image = data
                    completion(image)
                } else {
                    println("getImage5")
                    getStreetImageViewForCoordinates(completion)
                }
            }
        } else {
            getStreetImageViewForCoordinates(completion)
        }
    }

    private fun getStreetImageViewForCoordinates(completion: (image: ByteArray?) -> Unit) {
        if (location == null) {
            completion(null)
            return
        }
        doAsync {
            val urlString  = "https://maps.googleapis.com/maps/api/streetview?size=400x400&location=${location!!.latitude},${location!!.longitude}"
            val input = URL(urlString).openStream()

            val bitmap = BitmapFactory.decodeStream(input)
            var byteArray: ByteArray? = null
            if (bitmap != null) {
                val stream = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
                byteArray = stream.toByteArray()
            }
            uiThread {
                completion(byteArray)
            }
        }

    }

    companion object {

        fun loadSenderProperties() {
            Paper.book().delete(Constants.PaperKeys.senderProperties)
            Property.getSenderProperties(User.getCurrentUser(), { properties ->
                Paper.book().write(Constants.PaperKeys.senderProperties, properties)
            })
        }

        fun getQuery(): ParseQuery<ParseObject> {
            val query = ParseQuery<ParseObject>(Constants.PFColumns.Property.className)
            return query
        }

        fun getProperties(forQuery: ParseQuery<ParseObject>, completion: (properties: java.util.ArrayList<Property>) -> Unit) {
            forQuery.findInBackground { objects, e ->
                Unit
                val properties = ArrayList<Property>()
                if (objects != null) {
                    for (parseObject in objects) {
                        properties.add(Property(parseObject))
                    }
                }
                completion(properties)
            }
        }

        fun countProperties(forUser: String, completion: (numberOfProperties: Int) -> Unit) {
            val query = Property.getQuery()
            query.whereEqualTo(Constants.PFColumns.Property.receiverObjectId, forUser)
            query.countInBackground { count, e ->
                if (e == null){
                    completion(count)
                }else{
                    Log.e(TAG, "There was an error getting number of properties for user: "+e.message)
                    e.printStackTrace()
                }
            }
        }

        fun getFollowingMeProperties(forUser:User, completion: (properties:ArrayList<Property>?) -> Unit){
            val query = getQuery()
            val properties = ArrayList<Property>()
            query.whereContains(Constants.PFColumns.Property.senderObjectIds, forUser.objectId)
            query.findInBackground { objects, e ->
                if (e == null){
                    objects.iterator().forEach {
                        val property = Property(it)
                        properties.add(property)
                    }
                    completion(properties)
                } else {
                    Log.e(TAG, "There was an error getting following me properties: "+e.message)
                    e.printStackTrace()
                    completion(null)
                }
            }
        }

        fun getReceiverProperties(forUser: User, completion: (properties: java.util.ArrayList<Property>) -> Unit) {
            val query = getQuery()
            query.whereEqualTo(Constants.PFColumns.Property.receiverObjectId, forUser.objectId)
            getProperties(query, completion)
        }

        fun getSenderProperties(forUser: User, completion: (properties: java.util.ArrayList<Property>) -> Unit) {
            val query = getQuery()
            query.whereEqualTo(Constants.PFColumns.Property.senderObjectIds, forUser.objectId)
            getProperties(query, completion)
        }

        fun getSendersNotAtLocation(property: Property?, completion: (sendersNotAtLocation: ArrayList<User>) -> Unit) {
            val usersArray = ArrayList<User>()
            val searchedArray = ArrayList<String>()
            if (property != null) {
                searchedArray.addAll(property.senderObjectIds)
                searchedArray.removeAll(property.atLocationObjectIds)
                val query = ParseUser.getQuery()
                query.whereContainedIn(Constants.PFColumns.objectId, searchedArray)
                query.findInBackground { usersNotAtLocation, e ->
                    if (e == null) {
                        if (usersNotAtLocation != null) {
                            println("returned userNotAtLocation list size is: ${usersNotAtLocation.size}")
                            for (parseUser: ParseUser in usersNotAtLocation) {
                                println("${parseUser.getString(Constants.User.firstName)}")
                                usersArray.add(User(parseUser))
                            }
                            completion(usersArray)
                        }
                    } else {
                        Log.e(TAG, "There was an error getting senders NOT at location: " + e.message)
                        e.printStackTrace()
                    }
                }
            }
        }

        fun getSendersAtLocation(property: Property?, completion: (sendersNotAtLocation: ArrayList<User>) -> Unit) {
            val usersArray = ArrayList<User>()
            val searchedArray = ArrayList<String>()
            if (property != null) {
                searchedArray.addAll(property.atLocationObjectIds)
                println("getting Senders at location listz0rz is: ${property.atLocationObjectIds} and searchedArray has: $searchedArray")
                val query = ParseUser.getQuery()
                query.whereContainedIn(Constants.PFColumns.objectId, searchedArray)
                query.findInBackground { usersNotAtLocation, e ->
                    if (e == null) {
                        if (usersNotAtLocation != null) {
                            println("returned userNotAtLocation list size is: ${usersNotAtLocation.size}")
                            for (it in usersNotAtLocation) {
                                usersArray.add(User(it))
                            }
                            completion(usersArray)
                        }
                    } else {
                        Log.e(TAG, "There was an error getting senders NOT at location: " + e.message)
                        e.printStackTrace()
                    }
                }
            }
        }


        fun saveInformation(property: Property?, title: String, location: Location, addressString: String, image: ByteArray?, completion: (success: Boolean, property: Property?) -> Unit) {
            var parseObject = ParseObject(Constants.PFColumns.Property.className)
            if (property != null) {
                parseObject = ParseObject.createWithoutData(Constants.PFColumns.Property.className, property.objectId)
                parseObject.put(Constants.PFColumns.Property.senderObjectIds, property.senderObjectIds)
            }
            parseObject.put(Constants.PFColumns.Property.title, title)
            parseObject.put(Constants.PFColumns.Property.address, addressString)
            parseObject.put(Constants.PFColumns.Property.location, ParseGeoPoint(location.latitude, location.longitude))
            parseObject.put(Constants.PFColumns.Property.receiverObjectId, User.getCurrentUser().objectId)
            if (image != null) {
                val dateString = SimpleDateFormat("yyyyMMddhhssa").format(Date())
                val parseFile = ParseFile("window_property_image_" + dateString + ".png", image)
                parseObject.put(Constants.PFColumns.Property.imageFile, parseFile)
            }
            parseObject.saveInBackground { error ->
                Unit
                if (error == null) {
                    parseObject.fetchIfNeededInBackground { fetchedObject: ParseObject, e ->
                        if (fetchedObject != null) {
                            completion(true, Property(parseObject))
                        } else {
                            completion(true, null)
                        }
                    }
                } else {
                    completion(false, null)
                }
            }

        }
    }
}
