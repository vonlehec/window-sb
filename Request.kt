package com.netgalaxystudios.window

import com.parse.ParseException
import com.parse.ParseObject
import com.parse.ParseQuery
import io.paperdb.Paper
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by charlesvonlehe on 6/29/17.
 */

class Request(parseObject: ParseObject) {
    var objectId = ""
    var createdAt = Date()
    var property:Property? = null
    var fromObjectId:String? = null
    var toObjectId:String? = null
    var type = RequestType.Other
    private var fromUser:User? = null
    private var toUser:User? = null
    var accepted = false
    var declined = false

    init {
        if (parseObject.objectId != null) {
            objectId = parseObject.objectId
        }
        if (parseObject.createdAt != null) {
            createdAt = parseObject.createdAt
        }
        if (parseObject.getParseObject(Constants.PFColumns.Request.property) != null) {
            property = Property(parseObject.getParseObject(Constants.PFColumns.Request.property))
        }
        if (parseObject.getString(Constants.PFColumns.Request.fromObjectId) != null) {
            fromObjectId = parseObject.getString(Constants.PFColumns.Request.fromObjectId)
        }
        if (parseObject.getString(Constants.PFColumns.Request.toObjectId) != null) {
            toObjectId = parseObject.getString(Constants.PFColumns.Request.toObjectId)
        }
        if (parseObject.getString(Constants.PFColumns.Request.type) != null) {
            val requestType = parseObject.getString(Constants.PFColumns.Request.type).toUpperCase()
            if (requestType == "L") {
                type = RequestType.Location
            }else if (requestType == "B") {
                type = RequestType.BecomeSender
            }else if (requestType == "S") {
                type = RequestType.SenderArrived
            }else if (requestType == "D") {
                type = RequestType.SenderLeft
            }else {
                type = RequestType.Other
            }
        }
        accepted = parseObject.getBoolean(Constants.PFColumns.Request.accepted)
        declined = parseObject.getBoolean(Constants.PFColumns.Request.declined)
    }

    fun getUser (getFromUser:Boolean, completion: (user: User?) -> Unit) {
        if (getFromUser && fromUser != null) {
            completion(fromUser)
        }else if (!getFromUser && toUser != null) {
            completion(toUser)
        }
        var objId = toObjectId
        if (getFromUser) {
            objId = fromObjectId
        }
        if (objId != null) {
            User.getUserForObjectId(objId, {user ->
                if (getFromUser) {
                    fromUser = user
                }else {
                    toUser = user
                }
                completion(user)
            })
        }
    }

    fun respond (accept:Boolean, completion: (success: Boolean) -> Unit) {
        val parseObject = ParseObject.createWithoutData(Constants.PFColumns.Request.className, objectId)
        parseObject.put(Constants.PFColumns.Request.accepted, accept)
        parseObject.saveInBackground { e ->
            if (e == null) {
                if (fromObjectId != null) {
                    val objectIds = ArrayList<String>()
                    objectIds.add(fromObjectId!!)
                    if (accept) {
                        MethodHelper.sendPushToObjectIds(User.getCurrentUser().getNameString() + " accepted your request on Window.", objectIds, parseObject.objectId, null, null)
                    }
                    if (type == RequestType.BecomeSender) {
                        if (property != null) {
                            var senderProperties = Paper.book().read(Constants.PaperKeys.senderProperties, ArrayList<Property>())
                            senderProperties.add(property!!)
                            Paper.book().write(Constants.PaperKeys.senderProperties, senderProperties)
                            property?.addSender(User.getCurrentUser(), {success ->

                            })
                        }
                    }else if (type == RequestType.Location) {
                        MethodHelper.getCurrentLocation { location ->
                            if (location != null) {
                                User.updateCurrentUserLocation(location)
                            }
                        }
                    }
                }
                accepted = accept
                declined = !accept
            }
            completion(e == null)
        }
    }

    fun hide (forUser:User, completion: (success: Boolean) -> Unit) {
        val parseObject = ParseObject.createWithoutData(Constants.PFColumns.Request.className, objectId)
        parseObject.addUnique(Constants.PFColumns.Request.hiddenForUsers, forUser.objectId)
        parseObject.saveInBackground { e ->
            completion(e == null)
        }
    }

    companion object {
        fun getQuery():ParseQuery<ParseObject> {
            val query = ParseQuery.getQuery<ParseObject>(Constants.PFColumns.Request.className)
            query.include(Constants.PFColumns.Request.property)
            query.whereNotEqualTo(Constants.PFColumns.Request.hiddenForUsers, User.getCurrentUser().objectId)
            query.orderByDescending(Constants.PFColumns.createdAt)
            return query
        }

        fun getRequest(forObjectId:String, completion:(request:Request?)->Unit) {
            getQuery().getInBackground(forObjectId, {parseObject: ParseObject?, e: ParseException? ->
                if (parseObject != null) {
                    completion(Request(parseObject))
                }else {
                    completion(null)
                }
            })
        }

        fun getRequestsForCurrentUser (received:Boolean, completion:(requests:ArrayList<Request>)->Unit) {
            val query = getQuery()
            if (received) {
                query.whereEqualTo(Constants.PFColumns.Request.toObjectId, User.getCurrentUser().objectId)
            }else {
                query.whereDoesNotExist(Constants.PFColumns.Request.accepted)
                query.whereDoesNotExist(Constants.PFColumns.Request.declined)
                query.whereNotEqualTo(Constants.PFColumns.Request.type, Request.RequestType.SenderArrived.getValueCharacter())
                query.whereEqualTo(Constants.PFColumns.Request.fromObjectId, User.getCurrentUser().objectId)
            }
            val DAY_IN_MS : Long = 1000 * 60 * 60 * 24
            val weekAgo = Date().time - DAY_IN_MS
            query.whereGreaterThan(Constants.PFColumns.Request.createdAt, Date(weekAgo))
            query.findInBackground { objects, e ->
                val requests = ArrayList<Request>()
                if (objects != null) {
                    for (parseObject in objects) {
                        requests.add(Request(parseObject))
                    }
                }
                completion(requests)
            }
        }

        fun sendRequest (toUserObjectId:String, property:Property?, type:RequestType, completion:(success:Boolean)->Unit) {
            val parseObject = ParseObject.create(Constants.PFColumns.Request.className)
            parseObject.put(Constants.PFColumns.Request.toObjectId, toUserObjectId)
            parseObject.put(Constants.PFColumns.Request.fromObjectId, User.getCurrentUser().objectId)
            if (property != null) {
                parseObject.put(Constants.PFColumns.Request.property, ParseObject.createWithoutData(Constants.PFColumns.Property.className, property.objectId))
            }
            parseObject.put(Constants.PFColumns.Request.type, type.getValueCharacter())
            parseObject.saveInBackground { e ->
                if (e == null) {
                    var messageString = "You received a notification on Window"
                    if (type == RequestType.BecomeSender) {
                        messageString = User.getCurrentUser().getNameString() + " would like you to become a sender for their property on Window!"
                    }else if (type == RequestType.Location) {
                        messageString = User.getCurrentUser().getNameString() + " is requesting your location on Window. Tap here to accept."
                    }else if (type == RequestType.SenderArrived && property != null) {
                        messageString = User.getCurrentUser().getNameString() + " has arrived at " + property.title
                    }else if (type == RequestType.SenderLeft && property != null) {
                        messageString = User.getCurrentUser().getNameString() + " has just left " + property.title
                    }
                    val objectIds = ArrayList<String>()
                    objectIds.add(toUserObjectId)
                    MethodHelper.sendPushToObjectIds(messageString, objectIds, parseObject.objectId, null, null)
                }
                completion(e == null)
            }
        }


    }


    enum class RequestType {
        Location, BecomeSender, SenderArrived, Other, SenderLeft;

        fun getValueCharacter():String {
            if (this == Location) {
                return "L"
            }else if (this == BecomeSender) {
                return "B"
            }else if (this == SenderArrived) {
                return "S"
            }else if (this == SenderLeft) {
                return "D"
            }else {
                return "O"
            }
        }
    }
}
